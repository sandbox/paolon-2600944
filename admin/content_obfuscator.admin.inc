<?php

function content_obfuscator_admin_form($form, $form_state) {

  $form['content_obfuscator_selector'] = array(
    '#type' => 'textfield',
    '#title' => t('The CSS selector to target.'),
    '#description' => t("The obfuscator will look for text within the selector and the <strong>'p' tags</strong> underneath.<br>Better include the class of the article to be targeted and the view mode. For example: .node-article.view-mode-full"),
    '#default_value' => variable_get('content_obfuscator_selector', ''),
  );

   $form['content_obfuscator_images'] = array(
    '#type' => 'checkbox',
    '#title' => t('Do you want to obfuscate images as well?'),
    '#default_value' => variable_get('content_obfuscator_images', 0),
  );

  $form['content_obfuscator_popup_text'] = array(
    '#type' => 'textfield',
    '#title' => t('The text used to notify the user.'),
    '#default_value' => variable_get('content_obfuscator_popup_text', ''),
  );

  $options = array(
  	'none' => t('None'),
    'popup' => t('Popup'),
    'removal' => t('Text removal'),
    'blur' => t('Blur overlay'),
  );
  $form['content_obfuscator_effect'] = array(
    '#type' => 'select',
    '#title' => t('What effect to apply.'),
    '#description' => t('Only the "None" and "Popup" option will allow the user to continue reading the article with AD Blocker still on.'),
    '#options' => $options,
    '#default_value' => variable_get('content_obfuscator_effect', 'none'),
  );

  return system_settings_form($form);

}
