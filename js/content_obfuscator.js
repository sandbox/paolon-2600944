(function ($, Drupal) {
  if(typeof blockAdBlock === 'undefined') {
      adBlockDetected();
  } else {
      blockAdBlock.onDetected(adBlockDetected);
  }
  function adBlockDetected() {
    var selector = Drupal.settings.content_obfuscator.selector;
    var effect = Drupal.settings.content_obfuscator.effect;
    var images = Drupal.settings.content_obfuscator.images;
    if ($(selector).length > 0) {
      var blur = {
        "-webkit-filter": "blur(5px)",
        "-moz-filter": "blur(5px)",
        "-o-filter": "blur(5px)",
        "-ms-filter": "blur(5px)",
        "filter": "blur(5px)"
      };
      if (images) {
        $(selector + " img").css(blur);
      }
      switch (effect) {
        case 'popup':
          $('#content-obfuscator').fadeIn(800);
        case 'blur':
            $(selector + ", " + selector + " p").css({"text-shadow": "2px 2px 10px rgba(0,0,0,0.5)", "color": "transparent"});
        break;
        case 'removal':
          var text = Drupal.settings.content_obfuscator.text;
          $(selector + ' p').replaceWith('<div class="content-obfuscator-removed">' + text + '</div>');
        break;
      }
    }
  }
  function bounce(element, times, distance, speed) {
    for(var i = 0; i < times; i++) {
      element.animate({marginTop: '-=' + distance}, speed).animate({marginTop: '+=' + distance}, speed);
    }
  }
})(jQuery, Drupal);
